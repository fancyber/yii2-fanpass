<?php
namespace fancyber\passport\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use fancyber\passport\models\User;

class UserController extends Controller
{
    private $myName = 'fanpass';
    private $stateKey = 'fanpass_state';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


	public function actionLogin()
	{
		// read config
		$config = Yii::$app->getModule($this->myName);

        // generate state
        $state = md5(uniqid(rand(), true));
        Yii::$app->getSession()->set($this->stateKey, $state);

        // go to uni login
        $url = $config::LOGIN_URL
        	. '?i=' . $config->clientId
            . '&k=' . $state
            . '&c=' . urlencode(Yii::$app->urlManager->createAbsoluteUrl('/' . $this->myName . '/user/logas'));

        return $this->redirect($url);
	}

    public function actionLogas()
    {
        // check
        if(!isset($_REQUEST['i'])) Yii::$app->end();
        $uid=$_REQUEST['i'];
        if(!isset($_REQUEST['n'])) Yii::$app->end();
        $name=$_REQUEST['n'];
        if(!isset($_REQUEST['s'])) Yii::$app->end();
        $sign=$_REQUEST['s'];

        // read config
        $config = Yii::$app->getModule($this->myName);

        // verify
        $expect=md5($uid . $name . Yii::$app->getSession()->get($this->stateKey) . $config->privateKey);
        Yii::$app->getSession()->set($this->stateKey, NULL);

        if($expect != $sign) Yii::$app->end();

        if (!Yii::$app->getUser()->loginByAccessToken($uid))
        {
            if (!$config->allowNewUser)
            {
                Yii::$app->end();
            }

            // create new user record
            $model = new User;
            $model->uid = $uid;
            $model->name = $name;
            if (!$model->save() || !Yii::$app->getUser()->login($model, 3600 * 24 * 30))
            {
                Yii::$app->end();
            }
        }

        return $this->redirect(Yii::$app->user->returnUrl);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



}

