# Fancyber passport extension for Yii 2.0

Usage:

./yii migrate --migrationPath=@vendor/fancyber/yii2-fanpass/migrations

```
'user' => [
    'loginUrl' => array('fanpass/user/login'),
    'identityClass' => 'fancyber\passport\models\User',
],
```

```
'modules' => [
    'fanpass' => [
        'class' => 'fancyber\passport\Module',
        'clientId' => '<client_id>',
        'privateKey' => '<private_key>',
        'allowNewUser' => true, // optional, default true
    ],
],
```
