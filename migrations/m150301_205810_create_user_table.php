<?php

use yii\db\Schema;
use yii\db\Migration;

class m150301_205810_create_user_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('fanpass_user', [
            'id' => 'pk',
            'uid' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'created_ip' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_TIMESTAMP . ' NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => Schema::TYPE_TIMESTAMP . ' NULL',
        ], 'ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci');
        $this->createIndex('idx_uid', 'fanpass_user', 'uid', true);
    }

    public function safeDown()
    {
        $this->dropTable('fanpass_user');
    }
}
