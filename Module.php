<?php
namespace fancyber\passport;

use yii\base\InvalidConfigException;

class Module extends \yii\base\Module
{
    const STATE_KEY_PREFIX = 'fanpass_';
    const LOGIN_URL = 'https://www.10180.com/login';

//    public $controllerNamespace = 'fancyber\passport\controllers';

    public $clientId = '';
    public $privateKey = '';

    public $allowNewUser = true;

    public function init()
    {
        parent::init();

        if (empty($this->clientId) || empty($this->privateKey))
        {
            throw new InvalidConfigException("Please fill out clientId and privateKey");
        }
    }
}

